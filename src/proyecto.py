''' Proyecto eléctrico '''


import pandas as pd

'''
    Función encargada de extraer los datos de los archivos csv y hojas de
    cálculo para poder manipularlos por medio de Dataframes de pandas
'''


def ImportarDatos(archivo_logs, archivo_notas, archivo_fechas_tareas):
    # Lectura de archivo de logs
    logs = pd.read_csv(archivo_logs)

    # Filtro por hora

    # Primero se convierten las entradas de Hora de strings a timestamps
    logs['Hora'] = pd.to_datetime(logs['Hora'], format="%d/%m/%y, %H:%M")

    # Ahora se filtran las entradas del dataframe por fecha (Aprox. 1 marzo
    # 2023 - 1 agosto 2023)
    I2023 = logs[(logs['Hora'] > '2023-03-01') & (logs['Hora'] < '2023-08-01')]

    logs_sin_profesor = I2023[(I2023['Nombre completo del usuario'] != 'DIEGO \
        SAYED DUMANI JARQUIN') & (I2023['Nombre completo del usuario'] != ' \
        Administrador Soporte METICS')]

    # Lectura de archivo de notas
    notas = pd.read_excel(archivo_notas)
    # Se elimina información no relevante y se unen Nombre y apellido en una
    # sola columna
    notas.drop('Último descargado desde este curso', inplace=True, axis=1)
    notas['Nombre'] = notas['Nombre'] + ' ' + notas['Apellido(s)']
    notas.drop('Apellido(s)', inplace=True, axis=1)

    # Lectura de archivo con fechas de tareas
    fechas_tareas = pd.read_excel(archivo_fechas_tareas)
    # Se convierten las entradas de Hora de strings a timestamps
    fechas_tareas['Fecha entrega'] = pd.to_datetime(
        fechas_tareas['Fecha entrega'], format="%d/%m/%Y, %H:%M")

    # Se regresan los dataframes creados
    return (logs_sin_profesor, notas, fechas_tareas)


'''
    Primer Relación:
    Crea un dataframe en el que se muestra cada estudiante, su nota final del
    curso y la cantidad de veces que accesó a la página del curso
'''


def PrimerRelacion(logs, notas):
    # Se crera nuevo dataframe con nombres de los estudiantes del curso para
    # el semestre I 2023
    estudiantes = notas[['Nombre', 'Total del curso']]

    # Se filtran las entradas respectivas para cada estudiante para hacer un
    # conteo de entradas
    #  y se añaden al dataframe que se va a regresar como resultado
    lista = []
    for estudiante in estudiantes['Nombre']:
        lista.append(len(logs[(
            logs['Nombre completo del usuario'] == estudiante) &
            (logs['Nombre del evento'] == 'Curso visto')]))

    estudiantes['Cantidad de Accesos'] = pd.Series(lista)
    estudiantes = estudiantes.sort_values('Total del curso')
    return (estudiantes)


'''
    Segunda Relación:
    Cómo influye el ver la retroalimentación de una tarea a la siguiente
    las variables tarea1 y tarea2 corresponden a las tareas a evaluar.
    Se deben seleccionar por orden cronológico, siendo tarea1 la tarea
    más antigua y tarea2 la más reciente.
'''


def SegundaRelacion(logs, notas, tarea1, tarea2):
    lista = []

    tareas = notas[['Nombre', tarea1, tarea2]]
    tareas[tarea1] = pd.to_numeric(tareas[tarea1], errors='coerce').fillna(0)
    tareas[tarea2] = pd.to_numeric(tareas[tarea2], errors='coerce').fillna(0)

    for estudiante in tareas['Nombre']:
        lista.append(len(logs[(
            logs['Nombre completo del usuario'] == estudiante) &
            (logs['Contexto del evento'] == tarea1) &
            (logs['Nombre del evento'] == 'Retroalimentación vista')]))

    tareas['Accesos a retroalimentación'] = pd.Series(lista)
    tareas = tareas.sort_values('Accesos a retroalimentación')
    return (tareas)


'''
    Tercera Relación:
    Cómo se comporta una tarea específica respecto a la nota final del curso,
    esto para poder evidenciar qué tareas influyen más de cara a la nota final
'''


def TerceraRelacion(notas, tarea):
    resultados = notas[['Nombre', tarea, 'Total del curso']]
    resultados[tarea] = pd.to_numeric(
        resultados[tarea], errors='coerce').fillna(0)
    resultados = resultados.sort_values(tarea)
    return (resultados)


'''
    Cuarta Realción:
    Cómo afecta la hora de entrega a la nota de la tarea, esto para ver si
    el entregar una tarea con anticipación indica un mejor desempeño en la
    misma o si entregarla a ultima hora más bien mejora la calificación
'''


def CuartaRelacion(logs, notas, tarea, fechas_tareas):
    # Primero se filtran las entradas que sean relativas a la tarea en
    # cuestión y que cuyo contexto sea cargar la página de entrega de tarea
    filtro_entregas = logs[(
        logs['Nombre del evento'] == "El estatus del envío ha sido visto.") &
          (logs['Contexto del evento'] == tarea)]
    fecha_entrega = fechas_tareas[fechas_tareas['Tarea'] == tarea]
    fecha_entrega = fecha_entrega['Fecha entrega'].iloc[0]

    # Se crea nuevo dataframe con lista de estudiantes para agregar datos
    #  a exportar
    entregas = notas[['Nombre', tarea]]
    entregas[tarea] = pd.to_numeric(entregas[tarea], errors='coerce').fillna(0)

    lista = []
    cantidad_entregas = []
    tiempo_restante = []

    # Se recorre la lista completa de estudiantes y se busca las entregas a
    # la tarea que se está filtrando. Luego se añade a la lista la última
    # entrega que este estudiante hizo a la tarea. Igualmente se añade un
    # la cantidad de veces que el estudiante entregó la tarea en el caso de
    # haber modificado el envío
    for estudiante in entregas['Nombre']:
        filtro_estudiante = filtro_entregas[
            filtro_entregas['Nombre completo del usuario'] == estudiante]
        if filtro_estudiante.empty:
            lista.append(0)
            cantidad_entregas.append(0)
            tiempo_restante.append(0)

        else:
            lista.append(filtro_estudiante['Hora'].iloc[0])
            cantidad_entregas.append(len(filtro_estudiante['Hora']))
            tiempo_restante.append(
                (fecha_entrega - filtro_estudiante['Hora'].iloc[0]).seconds)

    entregas['Ultima modificación de entrega'] = pd.Series(lista)
    entregas['Tiempo restante (s)'] = pd.Series(tiempo_restante)
    entregas['Cantidad Entregas'] = pd.Series(cantidad_entregas)

    entregas = entregas.sort_values(tarea)

    return (entregas)


'''
    Función para exportar el Dataframe creado a un archivo csv que pueda
    abrirse desde excel para analizar los datos
'''


def ExportarDatos(datos_exportar):
    datos_exportar.to_csv(
        "//wsl.localhost/Ubuntu/home/lacriux/Proyecto/resultado.csv")


def main():
    # Primero se importan los archivos de datos como dataframes de pandas
    ubicacion_logs = "//wsl.localhost/Ubuntu/home/lacriux/Proyecto/logs.csv"
    ubicacion_notas = "//wsl.localhost/Ubuntu/home/lacriux/Proyecto/notas.xlsx"
    ubicacion_fechas_tareas = \
        "//wsl.localhost/Ubuntu/home/lacriux/Proyecto/fechas_tareas.xlsx"
    logs, notas, fechas_tareas = ImportarDatos(
        ubicacion_logs, ubicacion_notas, ubicacion_fechas_tareas)

    # Luego se escoge la Relación que se desea exportar comentando y
    # descomentando las siguientes funciones:

    # datos_exportar = PrimerRelacion(logs, notas)

    # Para la segunda relación es necesario brindar los nombres de las tareas a
    # evaluar, usando las siguientes variables:
    # tarea1 = 'Tarea: Tarea #3 - Cap 11'
    # tarea2 = 'Tarea: Tarea #4 - Cap 12'
    # datos_exportar = SegundaRelacion(logs, notas, tarea1, tarea2)

    # Para la tercera relación es necesario brindar los nombres de las tareas a
    # evaluar, usando las siguiente variable:
    # tarea = 'Tarea: Reporte LAFTLA #2 - Visita del martes 18/04 cap 10'
    # datos_exportar = TerceraRelacion(notas, tarea)

    # Para la cuarta relación se brinda el nombre de la tarea a evaluar:
    tarea = 'Tarea: Reporte LAFTLA #2 - Visita del martes 18/04 cap 10'
    datos_exportar = CuartaRelacion(logs, notas, tarea, fechas_tareas)

    ExportarDatos(datos_exportar)


if __name__ == "__main__":
    main()
