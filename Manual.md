# Manual de Uso

En este archivo se encuentra una explicación detallada de la herramienta, así como sus diferentes partes y su uso.

## Herramienta

La herramienta consiste en un programa en [Python](https://www.python.org/) que extrae de un archivo de logs en fomato .csv, que brinda la plataforma de mediación virtual de la Universidad de Costa Rica, los datos de las diferentes acciones realizadas tanto por estudiantes como profesores y el sistema de admisitración METICS sobre la plataforma de un curso. Estos datos crudos contienen información como la hora, el usuario, contexto, componenete, nombre del evento, descripción, origen y dirección ip de un evento realizado sobre la plataforma de un curso. Estos eventos pueden ser desde accesos a la página del curso hasta entregas de tareas, calificación de evaluaciones, revisión de retroalimentación, modificación de diversas entregas o secciones, entre otros.

Es importante destacar que esta información es cruda y se genera organiza por hora en la que sucedió el evento, de modo que es imposible extraer conclusiones relevantes de este archivo cuando el mismo consta de miles de líneas de datos.

En las secciones siguientes se va a explicar los pormenores del uso de la herramienta y al final se brindan recomendaciones de mejora de la misma.

## Trabajo Previo

Antes de poder poner en marcha la herramienta, es necesario instalar las dependencias de la misma, así como acondicionar los datos de entrada, ya que entre los archivos de entrada los datos se presentan de maneras diferentes, por lo que es necesario acndicionarlos para que la herramienta funcione sin problemas.

## Instalación
La instalación de la herramienta, así como sus dependencias puede ser encontrada en el archivo [README](https://gitlab.com/Lacriux1/proyecto-electrico-ii-2023/-/blob/main/README.md?ref_type=heads) que se encuentra en este repositorio de git.

## Acondicionamiento de Datos de Entrada

Como se mencionó en la sección de [Trabajo Previo](#trabajo-previo), es necesario acondicionar los datos de entrada, ya que entre los diferentes archivos de entrada: logs, lista de calificaciones y lista de fechas de entregas de tareas, no existe una coherencia entre los nombres de las tareas y evaluaciones, por lo que se imposibilita realizar los filtros que relacionan ambos archivos sin que se encuentren con nombres que sean consistentes entre ambos. También, el archivo de lista de fechas de entrega se crea a mano, ya que mediación brinda un PDF con una lista de fechas de entrega de evaluaciones del que es imposible extraer los datos necesarios.

Habiendo aclarado esto, las modificaciones necesarias a los archivos y el estándar que se va a utilizar los los siguientes:

- El archivo de logs lista las tareas y evaluaciones anteponiendo un 'Tarea: ', de modo que el nombre que aparece en los logs para las distintas evaluaciones sería 'Tarea: _nombre_de_la_tarea_'. mientras que el archivo de calificaciones muestra los nombres de las evaluaciones eliminando el espacio entre : y el nombre de la tarea, por lo que las evaluaciones aparecen 'Tarea:_nombre_de_la_tarea_'. De modo que para esta herramienta se va a utilizar el nombre a como aparece en el archivo de logs, por lo que es necesario modificar los nombres en el archivo de calificaciones y agregar este espacio a cada nombre de evaluación.
- El archivo de fechas de entrega se debe crear en una tabla de excel en la que venga una columna con el nombre de la tarea, siguiendo la misma nomenclatura mencionada en el punto anterior, y una columna con la fecha de entrega de la tarea en formato _DD/MM//YYYY, HH:MM_ (24H).

Luego de haber explicado el acondicionamiento de datos de entrada se procede a explicar el código de la herramienta.

## Código

Este programa se implementa en python3 y utiliza el estilo Flake8 de programación para facilitar su lectura. Consta de 7 métodos que se explican a continuación.

### Métodos

#### Main

El método main es el encargado de poder correr la herramienta y contiene todo el flujo principal de programa., se compone de las variables necesarias para llamar a los métodos que implementan la herramienta y la llamada a éstos métodos.

Lo primero que hay que tener en cuenta al usar la herramienta es que las ubicaciones de los archivos de entrada deben estar correctas y que estos archivos deben de existir.

#### ImportarDatos

Este método recibe todos los archivos de entrada (logs, notas y fechas de tareas) y los regresa como dataframes de pandas para poder ser procesados. Recibe la ubicación de los archivos como strings y regresa dataframes con la información de estos archivos.

Es imprtante mencionar que este método elimina información no relevante de los archivos que se estan imprtando, por ejemplo, lo primero que se hace eliminar las entradas que se encuentren fuera de un rango de fechas para poder filtrar la infomación relativa a un semestre específico, esto se logra convirtiendo la columna de hora a timestamps para luego filtrar los datos por un rango de fechas. Seguidamente se filtran las entradas para que solo aparezcan aquellas realizadas por estudiantes. Por lo que es necesario modificar esta sección con el nombre del profesor del curso para poder eliminar sus entradas.

Para el archivo de notas, se unen las entradas de _Nombre_ y _Apellido_ en una sola columna ya que así aparecen en el archivo de logs. 

Finalmente, para el archivo de fechas de entrega, se convierte la columna de fechas de un string a timestamps, de manera que se facilite la comparación por fechas.

#### ExportarDatos

Este método es muy sencillo, recibe un dataframe de pandas que contiene la informaciín que se va a exportar y escribe en un documento .csv dichos datos para que puedan ser imprortados luego a excel para su respectivo análisis. El nombre y la ubicación del archivo de resultados se pueden modificar en este método cambiando el string que representa la ubicación global de este archivo.

#### PrimeraRelación: Cantidad de Accesos y Nota Final

Este método recibe el dataframe de logs y el archivo de calificaciones ya acondicionados, lso filtra, acomoda y regresa un dataframe que contiene una lista con los nombres de los estudiantes, la calificación obtenida en el curso y la cantidad de accesos a la plataforma del curso en mediación virtual.

#### SegundaRelación: Retroalimentación entre una Evaluación y la Siguiente

Este método genera un dataframe que contiene la ista de estudiantes, una tabla que compara la nota entre 2 evaluaciones y la cantidad de veces que el estudiante accesó a la retroalimentación de la primera de las evaluaciones. Las evaluaciones deben recibirse en orden y sobre la primera de ellas es que se calcula la cantidad de accesos a retroalimentación. Como hacer eso se explica mejor en la sección de [Uso](#uso).

#### TerceraRelación: Calificación en Evaluación y Nota Final

Este método genera un dataframe en el que se muestra la lista de estudiantes y se compara la nota que tiene el estudiante contra la calificación final del curso. La tarea que se mustra en el dataframe se puede modificar mediante lo especificado en la sección de [Uso](#uso)

#### CuartaRelación: Calificación de Evaluación y Fecha de entrega

Este método es ligeramente más complejo que los anteriores, genera un dataframe que incluye la lista de estudiantes, una columna con la nota de cada estudiante para dicha evaluación, una columna con la fecha de la última modificación de la entrega, una lista con la cantidad de veces que el estudiante modificó la entrega de la evaluación y una columna que compara el tiempo restante que tenía el estudiante antes de que finalizara la entrega de la evaluación. Es muy importante destacar que pueden existir casos en los que los estudiantes modificaran la entrega de la evaluación después de vencido el tiempo de entrega, esto se ve reflejado en un valor negativo en la columna de tiempo restante. Al igual que para la tercera relación, la evaluación que se desea estudiar puede ser modificada, como se explica en la sección [Uso](#uso)

## Uso

En esta sección se explica como utilizar la herramienta para escoger cuál de las relaciones se quiere estudiar, así como escoger sobre cuál, o cuales, de las tareas se desea aplicar la relación.

Esto se realiza de manera muy sencilla: En la sección del método [main](#main) se encuentran comentadas las líneas de código que llaman a cada uno de los métodos que ejecutan las relaciones, de modo que la relación que se desea ejecutar es descomentada junto con el código axuliar de esta. Este código auxiliar es el que se encarga de escoger cuál de las tareas es la que se va a estudiar, por ejemplo, para correr la tercera relación sobre la segunda tarea el código quedaría de la siguiente forma:

```
# Luego se escoge la Relación que se desea exportar comentando y
# descomentando las siguientes funciones:

# datos_exportar = PrimerRelacion(logs, notas)

# Para la segunda relación es necesario brindar los nombres de las tareas a
# evaluar, usando las siguientes variables:
# tarea1 = 'Tarea: Tarea #3'
# tarea2 = 'Tarea: Tarea #4'
# datos_exportar = SegundaRelacion(logs, notas, tarea1, tarea2)

# Para la tercera relación es necesario brindar los nombres de las tareas a
# evaluar, usando las siguiente variable:
tarea = 'Tarea: Tarea #2'
datos_exportar = TerceraRelacion(notas, tarea)

# Para la cuarta relación se brinda el nombre de la tarea a evaluar:
# tarea = 'Tarea: Tarea #5'
# datos_exportar = CuartaRelacion(logs, notas, tarea, fechas_tareas)
```

De modo que en este ejemplo la relación que se ejecuta es la [tercera](#tercera-relación-calificación-en-evaluación-y-nota-final), y la tarea sobre la que se generan los resultados es la sgunda.

Para el caso específico de la segunda relación, es necesario brindar dos tareas. Es necesario que estas tareas sean consecutivas y que se ordenen cronológicamente, por ejemplo, si se quiere aplicar la relación sobre las tareas 5 y 6, el nombre de la tarea 5 se almacena en la variable _tarea1_ y el nombre de la tarea 6 se almacena en la variable _tarea2_ tal como se mustra en el siguiente código:

```
# Para la segunda relación es necesario brindar los nombres de las tareas a
# evaluar, usando las siguientes variables:
tarea1 = 'Tarea: Tarea #5'
tarea2 = 'Tarea: Tarea #6'
datos_exportar = SegundaRelacion(logs, notas, tarea1, tarea2)
```

Finalmente, con esto en cuenta se puede ejecutar la herramienta y los resultados de la relación aplicada se almacenan en el archivo de resultados que se define en el método [ExportarDatos](#exportardatos)

## Limitaciones  y Recomendaciones de mejora

Por último, como parte de este manual se incluyen recomendaciones sobre qué y cómo se puede mejorar esta herramienta, entre ellas se encuentran:

- El acondicionamiento de datos puede ser realizado en la herramienta misma, por medio de un método que realice los cambios sobre las diferencias que son detectadas.

- Es posible realizar el análisis de los resultados dentro de la misma herramienta, mediante la impresión de tablas y construcción de gráficos, pero para este proyecto resulta más sencillo realizar este análisis directamente en excel.

- Se puede expandir la cantidad de relaciones que puede realizar la herramienta, pero al moemento del desarrollo de la misma, estas 4 fueron las que se consideraron relevantes.

- Finalmente, se puede adaptar el código para que produzca resultados de todas las relaciones y sobre todas las posibles combinaciones de tareas en una sola ejecución del código, pero esto significaría una parametrización aún más grande de las diferentes variables que son parte de esta herramienta, así como la generación de lazos que puedan realizar estas ejecuciones múltiples y la parametrización de escritura de archivos de resultados.
