# Proyecto Eléctrico II-2023

Este proyecto implementa una herramienta sencilla de análisis de datos a partir de los logs de mediación virtual, tomando como entrada un archivo de logs en formato .csv, un archivo .xlsx con la lista de notas de las diferentes evaluaciones y un archivo .xlsx con la lista de las fechas de entrega de dichas evaluaciones. Consta de 6 métodos: 2 métodos para la importación y exportación de los datos y 4 métodos que crean dataframes para ordenar los datos a exportar.

## Instalación

Para poder utilizar esta herramienta son necesarias las siguientes dependencias:

python >= 3.10.12 ([Python3.10.12 Docs](https://www.python.org/downloads/release/python-31012/))
numpy >= 1.25.2 ([Numpy Docs](https://numpy.org/doc/stable/release/1.25.2-notes.html))
pandas >= 2.1.0 ([Pandas Docs](https://pandas.pydata.org/docs/whatsnew/v2.1.0.html))

Habiendo instalado python 3.10 se pueden instalar las demás dependencias con los siguientes comandos:

```
pip3 install numpy==1.25.2
pip3 install pandas==2.1.0
```

- Se puede eliminar la versión e instalar la última versión de ambos programas y la herramienta debería funcionar de la misma manera.

Una vez instaladas las dependencias se clona este repositorio y la herramienta se encuentra lista para su ejecución.

## Ejecución

Antes de ejecutar esta herramienta es necesario leer el manual que se encuentra en este repositorio, debido a que es necesario modificar el código fuente según los diferentes métodos que se van a ejecutar, además de que es necesario especificar en el código las ubicación de los distintos archivos de entrada que utiliza el programa.

## Notas

- Es necesario crear un directorio llamado 'data/' en donde se guardan los archivos con los datos crudos y modificar el código con los nombres de los archivos para que puedan ser cargados.
- Los resultados se guardan en un archivo .csv que contiene los datos ordenados para poder ser importados a excel y ser analizados ahí.
- Es importante leer el manual de uso, ya que explica de mejor forma como funciona el código y cualquier otro teme relevante.


